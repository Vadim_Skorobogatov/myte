from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
import time
import json
import re
import pdfkit
import os
from datetime import datetime
import random

start = time.time()

with open('input.json') as f:
    input_data = json.load(f)


def upload_file(driver,filenames,flag = "one"):
    first_file = filenames[0]
    n = len(driver.find_elements_by_xpath(
        "//table[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_grd_ExpenseSummary_NEW']/tbody/tr")) + 1
    n = str('0'+str(n)) if n<=9 else str(n)
    driver.find_element_by_xpath(
        f"//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_grd_ExpenseSummary_NEW_ctl{n}_HiddenUploader']").send_keys(
        first_file)
    time.sleep(3)
    try:
        element00 = driver.find_element_by_xpath(
            f"//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_grd_ExpenseSummary_NEW_ctl{n}_ImageElectronic']")
        if element00.get_attribute("alt") == "An electronic receipt is required for this expense.":
            driver.find_element_by_xpath(
                "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_ViewUploadReceiptsControl_fileupload1']").send_keys(
                first_file)
            time.sleep(2)
            driver.find_element_by_xpath(
                "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_ViewUploadReceiptsControl_btn_UploadReceipt']").click()
            driver.find_element_by_xpath(
                "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_btn_ReceiptsClose']").click()
    except:
        pass
    if flag == "many":
        remain_files = filenames[1:]
        for file in remain_files:
            element00 = driver.find_element_by_xpath(
                f"//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_grd_ExpenseSummary_NEW_ctl{n}_ImageElectronic']")
            element00.click()
            driver.find_element_by_xpath(
                "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_ViewUploadReceiptsControl_fileupload1']").send_keys(
                file)
            time.sleep(2)
            driver.find_element_by_xpath(
                "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_ViewUploadReceiptsControl_btn_UploadReceipt']").click()
            driver.find_element_by_xpath(
                "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_btn_ReceiptsClose']").click()
            time.sleep(1)

def login(driver,acc,password):
    flag_login = False

    driver.find_element_by_id("userNameInput").send_keys(acc)
    driver.find_element_by_id("passwordInput").send_keys(password)
    driver.find_element_by_id("submitButton").click()

    while not(flag_login):
        try:
            element1 = driver.find_element_by_xpath("//*[@id='vipOoblink']")
            element1.click()
            flag_login = True
        except:
            pass

    element2 = driver.find_element_by_xpath("//*[@id='vipSend']")
    element2.click()

    code = input("Enter code:")

    driver.find_element_by_id("otpInput").send_keys(code)

    element3 = driver.find_element_by_xpath("//*[@id='vipSubmitOTP']")
    element3.click()

    time.sleep(3)

def select_expenses(driver):
    element1 = driver.find_element_by_xpath("//*[@id='myExpenses_Card']/div[1]/h2/a")
    element1.click()

def add_hotel(driver,hotel,general):
    element1 = driver.find_element_by_xpath(
        "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_AddExpenseList_new_toggleDiv']")
    element1.click()

    element2 = driver.find_element_by_xpath(
        "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_tabContent']/div[2]/table/tbody/tr/td[12]/div[2]/ul/li[2]")
    element2.click()

    # select WBS
    element3 = driver.find_element_by_xpath(
        "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_expenseProjectDropDown']")
    element3.click()

    select0 = Select(driver.find_element_by_id('ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_expenseProjectDropDown'))
    select0.select_by_visible_text(general["WBS"])

    time.sleep(2)

    # select country
    element5 = driver.find_element_by_xpath(
        "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_8646']")
    element5.click()

    select1 = Select(driver.find_element_by_id('ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_8646'))
    select1.select_by_visible_text(hotel["country"])

    #select date
    #from
    element7 = driver.find_element_by_xpath(
        "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_expenseDate']")
    element7.send_keys(hotel["from"])
    #to
    element8 = driver.find_element_by_xpath(
        "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_8651']")
    element8.send_keys(hotel["to"])

    #select amount
    element9 = driver.find_element_by_xpath("//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_8638']")
    element9.send_keys(hotel["amount"])

    time.sleep(1)

    #select hotel chain
    element10 = driver.find_element_by_xpath("//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_8652']")
    element10.click()

    select2 = Select(driver.find_element_by_id('ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_8652'))
    select2.select_by_visible_text(hotel["chain"])

    # save
    element12 = driver.find_element_by_xpath("//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_btn_DetailOK_top']")
    element12.click()

    time.sleep(5)
    #upload documents
    filenames = [os.path.abspath(f"Hotel\{i}") for i in os.listdir("Hotel")]
    upload_file(driver,filenames,flag="many")

def add_per_diem(driver, general):
    element1 = driver.find_element_by_xpath(
        "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_AddExpenseList_new_toggleDiv']")
    element1.click()

    element2 = driver.find_element_by_xpath("//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_tabContent']/div[2]/table/tbody/tr/td[12]/div[2]/ul/li[8]")
    element2.click()

    # select WBS
    element3 = driver.find_element_by_xpath(
        "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_expenseProjectDropDown']")
    element3.click()

    select1 = Select(driver.find_element_by_id('ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_expenseProjectDropDown'))
    select1.select_by_visible_text(general["WBS"])

    time.sleep(2)

    #select date
    #from
    element4 = driver.find_element_by_xpath(
        "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_expenseDate']")
    element4.send_keys(general["from"])
    #to
    element5 = driver.find_element_by_xpath(
        "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_9737']")
    element5.send_keys(general["to"])

    time.sleep(2)

    #select type hotel/apart
    element6 = driver.find_element_by_xpath(
        "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_9743']")
    element6.click()
    time.sleep(2)
    select2 = Select(driver.find_element_by_id('ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_9743'))
    select2.select_by_visible_text("Hotel")

    time.sleep(2)

    # save
    element7 = driver.find_element_by_xpath("//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_btn_DetailOK_top']")
    element7.click()

    time.sleep(5)

    #upload documents
    filenames = [os.path.abspath(f"PerDiem\{i}") for i in os.listdir("PerDiem")]
    upload_file(driver,filenames,flag="many")


def yandex_taxi(file, bill, trips):
    text = file.read()
    pre_cost = re.search(r'<td>Итого<\/td><td>(\S*) ₽<', text)
    cost = pre_cost.group(1).split(',')[0]
    pre_date = re.search(r'<td>Смена N (\S*)', text)
    date = datetime.strptime(pre_date.group(1).split('>')[-1], '%d.%m.%y')
    date_for_save = pre_date.group(1).split('>')[-1].replace(".", "")
    converted_date = datetime.strftime(date, '%m/%d/%y')
    trips.append((cost, converted_date, str(cost) + "_" + str(date_for_save)))
    pdfkit.from_file(f'Taxi\{bill}', f'Taxi\{str(cost) + "_" + str(date_for_save)}.pdf')

def gett_taxi(file, bill, trips):
    text = file.read()
    pre_cost = re.search(r'<span class=" ">ИТОГО </span>(\S*)</div>', text)
    cost = pre_cost.group(1).split(',')[0].split('<')[0]
    pre_date = re.search(r'<div class="document_header_dateTime">(\S*) в ', text)
    date = datetime.strptime(pre_date.group(1).split('<')[0], '%d.%m.%Y')
    date_for_save = pre_date.group(1).split('>')[-1].replace(".", "")
    converted_date = datetime.strftime(date, '%m/%d/%y')
    trips.append((cost, converted_date, str(cost) + "_" + str(date_for_save)))

    link_bill = re.search(r'https(\S*)', text).group(0)
    bill_driver = webdriver.Chrome()
    bill_driver.maximize_window()
    bill_driver.get(link_bill)
    bill_driver.save_screenshot(f'Taxi\{str(cost) + "_" + str(date_for_save)}.png')
    bill_driver.quit()

def add_taxi(driver,general):
    #Get date and amount from bills, generate pdf
    trips = []
    bills = os.listdir("Taxi")
    for bill in bills:
        if bill.endswith(".html"):
            with open(f"Taxi\{bill}", "r", encoding='utf-8') as f:
                try:
                    yandex_taxi(f,bill,trips)
                except:
                    gett_taxi(f, bill, trips)


    #Send data to myte

    for trip in trips:
        #Select taxi
        element1 = driver.find_element_by_xpath(
            "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_AddExpenseList_new_toggleDiv']")
        element1.click()

        element2 = driver.find_element_by_xpath("//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_tabContent']/div[2]/table/tbody/tr/td[12]/div[2]/ul/li[14]")
        element2.click()

        # select WBS
        element3 = driver.find_element_by_xpath(
            "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_expenseProjectDropDown']")
        element3.click()

        select1 = Select(driver.find_element_by_id('ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_expenseProjectDropDown'))
        select1.select_by_visible_text(general["WBS"])

        time.sleep(1)

        #select country
        element4 = driver.find_element_by_xpath(
            "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_8469']")
        element4.click()

        select2 = Select(driver.find_element_by_id(
            'ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_8469'))
        select2.select_by_visible_text(general["country"])

        #select date
        element5 = driver.find_element_by_xpath(
            "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_expenseDate']")
        element5.send_keys(trip[1])

        time.sleep(1)

        #select reason
        reason = random.randint(9,11)
        element6 = driver.find_element_by_xpath("//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_8471']")
        element6.click()

        element7 = driver.find_element_by_xpath(f"//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_8471']/option[{reason}]")
        element7.click()

        time.sleep(1)

        #select teaxable
        element8 = driver.find_element_by_xpath("//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_8481']")
        element8.click()

        time.sleep(1)

        #select murk up
        element9 = driver.find_element_by_xpath("//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_8482']")
        element9.click()

        element10 = driver.find_element_by_xpath("//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_8482']/option[3]")
        element10.click()

        #set amount
        element11 = driver.find_element_by_xpath(
            "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_8467']").clear()
        # element11.send_keys(trip[0])
        driver.find_element_by_xpath(
            "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_expense_DetailsControl_8467']").send_keys(trip[0])

        #save
        element12 = driver.find_element_by_xpath(
            "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_btn_DetailOK_top']")
        element12.click()
        time.sleep(3)

        #upload check
        filename = [os.path.abspath(f"Taxi\{trip[2]}.pdf")]
        try:
            upload_file(driver, filename)
        except:
            filename = [os.path.abspath(f"Taxi\{trip[2]}.png")]
            upload_file(driver, filename)

def add_express(driver,general):
    # Select trail
    element1 = driver.find_element_by_xpath(
        "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_AddExpenseList_new_toggleDiv']")
    element1.click()

    element2 = driver.find_element_by_xpath(
        "//*[@id='ctl00_ctl00_MainContentPlaceHolder_ContentPlaceHolder_TimeReport_tabContent']/div[2]/table/tbody/tr/td[12]/div[2]/ul/li[14]")
    element2.click()

def press_ok(driver):
    driver.find_element_by_xpath("//*[@id='missedTRs']/div/input").click()

def press_ok_windows(driver):
    driver.find_element_by_xpath("//*[@id='idSIButton9']").click()

flag_ok = 0
driver = webdriver.Chrome()
driver.get("https://myte.accenture.com")

general = input_data["general"]
hotel = input_data["hotel"]
options = input_data["options"]

login(driver,acc = general["account"],password = general["pass"])
time.sleep(2)
while flag_ok == 0:
    try:
        press_ok_windows(driver)
        flag_ok = 1
    except:
        pass
time.sleep(2)
select_expenses(driver)
time.sleep(1)
if options["hotel"]:
    add_hotel(driver,hotel,general)
    time.sleep(2)

if options["per_diem"]:
    add_per_diem(driver,general)
    time.sleep(2)

if options["taxi"]:
    add_taxi(driver,general)
finish = time.time()

print(finish - start)

